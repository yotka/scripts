package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"golang.org/x/exp/slices"
)

func main() {
	sxivFlags := "-toa"
	recursiveFlag := false
	if slices.Contains(os.Args, "-r") {
		recursiveFlag = true
	}

	if recursiveFlag {
		sxivFlags += "r"
	}

	cmd := exec.Command("nsxiv", sxivFlags, os.Args[1])
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()
	outbuf := new(bytes.Buffer)
	errbuf := new(bytes.Buffer)

	cmd.Start()
	outbuf.ReadFrom(stdout)
	cmd.Wait()
	files := strings.Split(strings.TrimSpace(outbuf.String()), "\n")
	files = uniq(files)

	var args []string
	args = append(args, "-toa")
	args = append(args, files...)

	if len(files) == 1 {
		os.Exit(1)
	}

	cmd = exec.Command("nsxiv", args...)

	stdout, _ = cmd.StdoutPipe()
	stderr, _ = cmd.StderrPipe()
	outbuf.Reset()
	errbuf.Reset()

	cmd.Start()
	outbuf.ReadFrom(stdout)
	errbuf.ReadFrom(stderr)
	cmd.Wait()

	fmt.Printf("%v", outbuf)
	if errbuf.Len() != 0 {
		fmt.Printf("\033[31m%v\n\033[0m", errbuf)
	}
}

func uniq(s []string) []string {
	inResult := make(map[string]bool)
	var result []string
	for _, str := range s {
		if _, ok := inResult[str]; !ok {
			inResult[str] = true
			result = append(result, str)
		}
	}
	return result
}
