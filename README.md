# Лично используемые скрипты для всякой мелочи, которую можно было бы сделать и нормально, но cделано как сделано
`exported` - скрипты, помещённые в path  
Всё остальное не используется руками, а запускается иным софтом, либо на хоткеях

## Моё

### sxivtrash
Подразумевается использование в подобной связке:  
`sxivtrash . | xargs -d'\n' trash --`

## Не моё
- bashmount ([GPLv2](https://github.com/jamielinux/bashmount))
- pfetch ([MIT](https://github.com/dylanaraps/pfetch))
- yaya/yayaya - `¯\_(ツ)_/¯`

